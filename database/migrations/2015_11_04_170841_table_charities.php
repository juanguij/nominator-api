<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCharities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('charity_categories')->onDelete('cascade');
            $table->mediumText('description');
            $table->string('city', 64);
            $table->boolean('featured');
            $table->string('logo', 128);
            $table->string('website', 64);
            $table->string('wiki', 128);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('charities');
    }
}
