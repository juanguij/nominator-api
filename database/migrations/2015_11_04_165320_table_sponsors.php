<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableSponsors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors', function(Blueprint $table){
            $table->increments('id');
            $table->integer('nomination_id')->unsigned();
            $table->foreign('nomination_id')->references('id')->on('nominations')->onDelete('cascade');
            $table->string('name');
            $table->binary('hide_name');
            $table->mediumText('message');
            $table->float('amount');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('postal_code', 12);
            $table->string('payment_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sponsors');
    }
}
