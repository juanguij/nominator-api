<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table){
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('initial', 2);
            $table->string('password');
            $table->string('email')->unique();
            $table->text('description');
            $table->string('location');
            $table->float('latitude');
            $table->float('longitude');
            $table->boolean('custom_dp');
            $table->string('custom_dp_path');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
