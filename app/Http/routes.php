<?php


$app->group(['prefix'=>'api/v1', 'namespace' => 'App\Http\Controllers'], function () use ($app) {

    // Authentication APIs
    $app->post('/auth/login', 'AuthController@postLogin');
    $app->delete('/auth/logout', ['middleware' => 'auth:api', 'uses'=>'AuthController@logout']);

});

$app->group(['prefix'=>'api/v1/users', 'namespace' => 'App\Http\Controllers'], function () use ($app) {

    // User related APIs
    $app->get('/', ['uses' => 'UsersController@index']);
    $app->post('/', ['uses' => 'UsersController@store']);
    $app->get('/{id}', ['uses' => 'UsersController@show']);
    $app->put('/{id}', ['middleware' => 'auth:api', 'uses' => 'UsersController@update']);
    $app->delete('/{id}', ['uses' => 'UsersController@destroy']);

    // Follower related APIs
    $app->get('/{id}/followers', ['uses'=>'FollowersController@followers']);
    $app->get('/{id}/followings', ['uses'=>'FollowersController@followings']);
    $app->get('/{id}/followings', ['uses'=>'FollowersController@followings']);
    $app->get('/{id}/kpis', ['uses'=>'FollowersController@kpis']);
    $app->post('/{id}/follow', ['middleware' => 'auth:api', 'uses'=>'FollowersController@follow']);
});

$app->group(['prefix'=>'api/v1/nominations', 'namespace' => 'App\Http\Controllers'], function () use ($app) {

    // Nomination related APIs
    $app->get('/', ['uses'=>'NominationsController@index']);
    $app->post('/', ['middleware' => 'auth:api', 'uses'=>'NominationsController@store']);
    $app->get('/{id}', ['uses'=>'NominationsController@show']);
    $app->put('/{id}', ['middleware' => 'auth:api', 'uses'=>'NominationsController@update']);
    $app->delete('/{id}', ['middleware' => 'auth:api', 'uses'=>'NominationsController@destroy']);
    $app->patch('/{id}/publish', ['middleware' => 'auth:api', 'uses'=>'NominationsController@publish']);
    $app->patch('/{id}/unpublish', ['middleware' => 'auth:api', 'uses'=>'NominationsController@unpublish']);

    //Nomination Updates Related APIs
    $app->get('/{id}/updates', ['uses'=>'UpdatesController@index']);
    $app->put('/{id}/updates', ['middleware' => 'auth:api', 'uses'=>'UpdatesController@store']);
    $app->get('/{id}/updates/{update_id}', ['uses'=>'UpdatesController@show']);
    $app->patch('/{id}/updates/{update_id}', ['middleware' => 'auth:api', 'uses'=>'UpdatesController@update']);
    $app->delete('/{id}/updates/{update_id}', ['middleware' => 'auth:api', 'uses'=>'UpdatesController@destroy']);


    // Renomination related APIs
    $app->get('/{id}/kpi', ['uses'=>'RenominationsController@kpi']);
    $app->get('/{id}/renominations', ['uses'=>'RenominationsController@index']);
    $app->post('/{id}/renominate', ['middleware' => 'auth:api', 'uses'=>'RenominationsController@renominate']);
});


$app->group(['prefix'=>'api/v1/resources', 'namespace' => 'App\Http\Controllers'], function () use ($app) {

    // Resources related APIs
    $app->post('/upload/logo', ['middleware' => 'auth:api', "uses"=>'UploadsController@logo']);
    $app->post('/upload/image', ['middleware' => 'auth:api', "uses"=>'UploadsController@image']);

});
