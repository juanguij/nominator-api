<?php

namespace App\Http\Controllers;

use Acme\Likeability;
use App\Like;
use App\Nomination;
use Illuminate\Http\Request;

class RenominationsController extends Controller
{

    use Likeability;

    /**
     * Get the list of all renominations for a nomination with id = $id
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($id, Request $request)
    {
        $renominations = Nomination::findorfail($id)->renominations;

        return response()->json($renominations);
    }




    /**
     * Renominate a nomination
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renominate($id, Request $request)
    {
        $nomination = Nomination::findorfail($id);

        $nomination->toggleRenomination();

        return response()->json($nomination->renominated_users(5));
    }


    /**
     * Get the KPI's for the nomination
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function kpi($id)
    {
        $renominations = Nomination::findorfail($id)->renominations;

        return response()->json(['count'=>["renominations"=>count($renominations)]]);
    }


}
