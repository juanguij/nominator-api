<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsersController extends Controller
{


    /**
     * Get the list of all users
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json(['users'=>$users]);
    }


    /**
     * Show a particular user
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        $user = User::with('nominations', 'followers', 'following')->findOrFail($id);
        return response()->json($user);
    }


    /**
     * Create a user and return the object
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $request['password'] = (new BcryptHasher)->make($request->input('password'));
        $user = User::create($request->all());
        return response()->json($user);
    }


    /**
     * Update the user information
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id, Request $request)
    {

        $auth = JWTAuth::parseToken()->authenticate();

        if($auth->id!=$id) return response()->json('Operation not allowed', 403);

        $this->validate($request, [
            'username'  => 'unique:users',
            'email'     => 'email|max:255|unique:users',
            'password'  => 'min:6|confirmed'
        ]);

        User::findOrFail($id)->update($request->all());

        return response()->json(User::find($id));
    }


    /**
     * Delete a particular user
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return response()->json('deleted');
    }

}
