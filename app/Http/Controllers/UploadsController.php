<?php

namespace App\Http\Controllers;

use App\BBImage;
use Illuminate\Http\Request;
use App\BBLogo;
use Illuminate\Http\UploadedFile;

class UploadsController extends Controller
{


    /**
     * Upload a logo for the nomination and return the path.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logo(Request $request)
    {

        $this->validate($request, [
            'file' => 'required|max:1000|file|image|dimensions:max_width=200,max_height=60,min_width=75'
        ]);

        try{
            $logo = (new BBLogo($request->file('file')))->makePhoto();

            return response()->json($logo);
        }
        catch(\Exception $e){

            return response()->json("Could not upload", 500);

        }

    }




    /**
     * Upload an image for the nomination and return the path.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function image(Request $request)
    {

        $this->validate($request, [
            'file' => 'required|max:3000|file|image|dimensions:min_width=800,min_height:400'
        ]);

        try{
            $image = (new BBImage($request->file('file')))->makePhoto();

            return response()->json($image);
        }
        catch(\Exception $e){

            return response()->json("Could not upload", 500);

        }

    }


}
