<?php

namespace App\Http\Controllers;

use Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Nomination;
use Illuminate\Http\Request;

class NominationsController extends Controller
{


    /**
     * Get the list of all nominations
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $nominations = collect(Nomination::all())->toArray();

        return response()->json(["nominations"=>$nominations]);
    }


    /**
     * Get the nomination with the specific id
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id, Request $request)
    {
        $nomination = Nomination::findOrFail($id);

        return response()->json($nomination);
    }


    /**
     * Create a nomination and pass it back as a response.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'place' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'image' => 'required|url',
            'description' => 'required'
        ]);

        $nomination = Nomination::createNew($request);

        return response()->json($nomination);
    }


    /**
     * Delete a nomination with the specific id.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $nomination = Nomination::findorfail($id);

        if($this->getAuthId() == $nomination->user_id)
        {
            $nomination->delete();
            return response()->json('nomination deleted');
        }
        return response()->json('Operation not allowed', 403);
    }


    /**
     * Update a nomination with the given id.
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id, Request $request)
    {
        $nomination = Nomination::findorfail($id);

        if($this->getAuthId() == $nomination->user_id)
        {
            $updated = $nomination->updateNomination($request);
            return response()->json($updated);
        }
        return response()->json('Operation not allowed', 403);
    }


    /**
     * Publish a nomination
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function publish($id)
    {
        $nomination = Nomination::findorfail($id);

        if($this->getAuthId() == $nomination->user_id)
        {
            $nomination->update(['published'=>true]);

            return response()->json('published');
        }
        return response()->json('Operation not allowed', 403);

    }




    /**
     * Unpublish a nomination
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unpublish($id)
    {
        $nomination = Nomination::findorfail($id);

        if($this->getAuthId() == $nomination->user_id)
        {
            $nomination->update(['published'=>false]);

            return response()->json('unpublished');
        }
        return response()->json('Operation not allowed', 403);

    }

    /**
     * Get the authenticated user id.
     *
     * @return null
     */
    protected function getAuthId()
    {
        return (JWTAuth::getToken()) ? JWTAuth::parseToken()->authenticate()->id : null;
    }

}
