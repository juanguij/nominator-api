<?php

namespace App\Http\Controllers;


use App\Update;
use Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Nomination;
use Illuminate\Http\Request;

class UpdatesController extends Controller
{


    /**
     * Show the list of all updates for a nomination
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($id)
    {
        $updates = Nomination::findorfail($id)->updates;

        return response()->json($updates);
    }


    /**
     * Create a new update for the nomination
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store($id, Request $request)
    {

        $this->validate($request, [
            'message' => 'required'
        ]);

        $nomination = Nomination::findorfail($id);

        if($nomination->user_id!=$this->getAuthId()) return response()->json('Operation not allowed', 403);

        $update = $nomination->updates()->create([
            'user_id'   =>  $this->getAuthId(),
            'message'   =>  $request->input('message')
        ]);

        return response()->json($update);
    }


    /**
     * Show the update with the id along with the parent nomination
     *
     * @param $id
     * @param $update_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($id, $update_id)
    {
        $update = Update::with('nomination')->findorfail($update_id);

        return response()->json($update);
    }


    /**
     * Update the update of a nomination
     *
     * @param $id
     * @param $update_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id, $update_id, Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $update = Update::findorfail($id);

        if($update->user_id!=$this->getAuthId()) return response()->json('Operation not allowed', 403);

        $update->update([
            'message'   =>  $request->input('message')
        ]);

        return response()->json(Update::findorfail($id));
    }


    /**
     * Delete an update with the given id.
     *
     * @param $id
     * @param $update_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id, $update_id)
    {
        $update = Update::findorfail($update_id);

        if($update->user_id!=$this->getAuthId()) return response()->json('Operation not allowed', 403);

        $update->delete();

        return response()->json('deleted');
    }


    /**
     * Get the authenticated user id.
     *
     * @return null
     */
    protected function getAuthId()
    {
        return (JWTAuth::getToken()) ? JWTAuth::parseToken()->authenticate()->id : null;
    }

}