<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class FollowersController extends Controller
{

    public $user;

    public function __construct()
    {
        if(JWTAuth::getToken())
        {
            if (! $this->user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        }
    }

    /**
     * Get the list of all followers for the user id provided
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function followers($id)
    {
        $user = User::findOrFail($id);

        $followers = $user->followers()->get();

        return response()->json($followers, 200);
    }


    /**
     * Get the list of all followings for the id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function followings($id)
    {
        $user = User::findOrFail($id);

        $followings = $user->following()->get();

        return response()->json($followings, 200);
    }


    /**
     * Follow a particular user with the $id.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function follow($id)
    {
        if ($this->user->id == $id ) return response()->json('Cannot follow yourself', 422);

        $follow = User::findOrFail($id);

        $follow->toggleFollow();

        return response()->json([], 200);
    }


    /**
     * Get the count of followers and followings for the user with id
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function kpis($id)
    {
        $user = User::findOrFail($id);

        return response()->json(["count"=>["followers" => $user->followers()->count(), "following" => $user->following()->count()]], 200);
    }

}
