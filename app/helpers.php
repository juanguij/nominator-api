<?php

function avatar($user)
{
    return ($user->custom_dp) ? ($user->custom_dp_path) : ("https://graph.facebook.com/$user->facebook_id/picture?height=300");
}


if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}