<?php namespace App\Billing;

use Stripe\Error\ApiConnection;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Error\InvalidRequest;
use Stripe\Token;

class StripeGateway
{

    protected $customer;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRETE_KEY'));
    }

    /**
     * Charge a user
     * @param $customer_id
     * @param $amount
     * @return array
     */
    public function charge($email, $stripe_token, $amount, $currency)
    {
            $customer = $this->createCustomer($email, $stripe_token);

            if(!$customer['success'])
            {
                return $customer;
            }

            return $this->chargeExisting($customer['customer']->id, $customer['customer']['sources']['data'][0]->id, $amount, $currency);
    }


    /**
     * Charge an existing card
     * @param $customer_id
     * @param $amount
     * @return array
     */
    public function chargeExisting($customer_id, $stripe_card, $amount, $currency)
    {
        try
        {
            $charge = Charge::create([
                'amount' => $amount*100,
                'currency' => $currency,
                'card' => $stripe_card,
                'customer' => $customer_id
            ]);

            return ['success'=>true, 'billing'=>$charge, 'customer'=>$customer_id];
        }
        catch(InvalidRequest $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(Card $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(ApiConnection $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
    }

    /**
     * Create a new customer
     * @param $email
     * @return Customer
     */
    public function createCustomer($email, $token)
    {
        try {
            $this->customer = Customer::create(['email' => $email, 'source' => $token]);

            return ['success'=>true, 'customer'=>$this->customer];
        }
        catch(InvalidRequest $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(Card $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(ApiConnection $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }

    }

    /**
     * Retrieve the customer
     * @param $email
     * @return Customer
     */
    public function getCustomer($customer_id)
    {
        $this->customer = Customer::retrieve($customer_id);
        return $this->customer;
    }


    /**
     * Creating a new Card
     * @param $customer_id
     * @param $token
     * @return mixed
     */
    public function newCard($customer_id, $token)
    {
        try
        {
            $customer = $this->getCustomer($customer_id);

            $card = $customer->sources->create(['source'=>$token]);

            return ['success'=>true, 'card'=>$card];
        }
        catch(InvalidRequest $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(Card $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(ApiConnection $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
    }


    /**
     * Creating a new Card
     * @param $customer_id
     * @param $token
     * @return mixed
     */
    public function deleteCard($customer_id, $card_id)
    {
        try
        {
            $customer = $this->getCustomer($customer_id);

            $card_deleted = $customer->sources->retrieve($card_id)->delete();

            return ['success'=>true, 'deleted'=>$card_deleted];
        }
        catch(InvalidRequest $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(Card $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
        catch(ApiConnection $e)
        {
            return (['success'=>false, 'error'=>$e->getMessage()]);
        }
    }


    /**
     * Returning Fake Stripe Token
     * @return null
     */
    public function fakeToken()
    {
        return Token::create([
            "card" => [
                "number" => "4242424242424242",
                "exp_month" => 1,
                "exp_year" => 2019,
                "cvc" => "314"
            ]
        ])->id;
    }

}