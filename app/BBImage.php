<?php namespace App;

use Response;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BBImage
{

    protected $file;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }


    public function makePhoto()
    {
        $path = 'nominations/photos/';

        $name = $this->makeFileName();

        $this->file->move($path, $name);

        return ('/'.$path.$name);
    }



    protected function makeFileName()
    {
        $name = sha1(
            time() . $this->file->getClientOriginalName()
        );

        $extension = $this->file->getClientOriginalExtension();

        return $name.'.'.$extension;

    }


}