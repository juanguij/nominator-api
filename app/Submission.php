<?php namespace App;


use Illuminate\Http\Request;

class Submission extends Nomination
{


    /**
     * Save Property Location Details in the session received from the Form.
     * @param Request $request
     */
    public function property(Request $request)
    {
        $property =
            (object)[
                'location' => (object) [
                    'place' => $request->input('property-name'),
                    'type' => $request->input('property-icon'),
                    'lng' => $request->input('property-long'),
                    'lat' => $request->input('property-lat'),
                    'address' =>  (object) [
                        'country' => $request->input('property-country'),
                        'state' => $request->input('property-state'),
                        'city' => $request->input('property-city')
                    ]
                ]
            ];

        session()->put('property', $property);

    }


    /**
     * Get the nomination details for the submission.
     * @param Request $request
     */
    public function nomination(Request $request)
    {
        $nomination = (object) [
            'name' => $request->input('nomination'),
            'type' => $request->input('nomination-type')
        ];

        session()->put('nomination', $nomination);
    }




    public function image(Request $request)
    {
        $image =    (object)[
                        'type' => $request->input('image_type'),
                        'path' => $request->input('image'),
                        'flickr_image_id' => $request->input('flickr_image_id'),
                        'licence' => 1,
                        'offset' => (object)[
                            'cover' => $request->input('v_offset'),
                            'box' => $request->input('h_offset')
                        ]
                    ];

        session()->put('image', $image);
    }


    public function branding(Request $request)
    {
        $branding = (object)[
            'type' => $request->input('brand_type'),
            'font' => $request->input('font'),
            'logo' => $request->input('logo'),
            'h_offset' => $request->input('h_offset')
        ];

        session()->put('branding', $branding);

    }



    public function description(Request $request)
    {
        session()->put('description', nl2br($request->input('describe')));
    }


    public function review(Request $request)
    {
        $review = (object)[
            'flickr_author' => $request->input('flickrAuthor'),
            'flickr_license' => $request->input('flickrLicense'),
            'v_offset' => $request->input('v_offset')
        ];

        session()->put('review', $review);
    }



    /**
     * Session variable for the Nomination Creation Process.
     * @param Request $request
     */
    public function saveToSession(Request $request)
    {
        $submission = [

            'name' => $request->input('name'),

            'place' => $request->input('search_property'),

            'description' => $request->input('description'),


            'funding' => [
                'type' => $request->input('funding_type'),
                'amount' => $request->input('amount'),
                'period' => 0
            ],

            'options' => [
                    'branding' => [
                        'type' => $request->input('branding_type'),
                        'font' => $request->input('font'),
                        'logo' => $request->input('logo')
                    ],
                    'image' => [
                        'type' => $request->input('image_type'),
                        'path' => $request->input('image'),
                        'offset' => [
                            'cover' => $request->input('v_offset'),
                            'box' => $request->input('h_offset')
                        ],
                        'owner' => $request->input('flickr_user'),
                        'licence' => 1
                    ]
            ],

            'featured' => false,

            'published' => true

        ];

        session()->put('submission', $submission);
    }


}