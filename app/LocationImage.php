<?php namespace App;

class LocationImage
{
    protected $place;

    protected $page;

    protected $url;

    public function __construct($place=null, $page=null)
    {
        $this->place = $place;

        $this->page = $page;
    }

    public function getImage()
    {
        error_reporting(E_ALL);

        $this->url = $this->setFlickrURL();

        $response = $this->callFlickr();

        return $response;

    }


    protected function setFlickrURL()
    {
        $api_key = env('FLICKR_API');
        $text  = $this->place;
        $perPage = 20;
        $url = env('FLICKR_REQUEST_URL');
        $url.= '&api_key='.$api_key;
        $url.= '&text='.urlencode($text);
        $url.= '&sort=relevance';
        $url.= '&per_page='.$perPage;
        $url.= '&format=json';
        $url.= '&nojsoncallback=1';
        $url.= '&page='.$this->page;

        return $url;
    }


    protected function callFlickr()
    {

        return json_decode(file_get_contents($this->url));
    }


    protected function arrangeImage($response)
    {
        $photo_array = $response->photos->photo;

        $count=0;
        $img = [];
        foreach($photo_array as $single_photo){
            $size = 's';
            $size2='b';
            $photo_url = 'https://farm'.$single_photo->farm.'.staticflickr.com/'.$single_photo->server.'/'.$single_photo->id.'_'.$single_photo->secret.'_'.$size.'.'.'jpg';
            $photo_url_big = 'https://farm'.$single_photo->farm.'.staticflickr.com/'.$single_photo->server.'/'.$single_photo->id.'_'.$single_photo->secret.'_'.$size2.'.'.'jpg';

            $img[$count] = $photo_url;
            $largeImg[$count] =$photo_url_big;
            $imageId[$count] = $single_photo->id;
            $count++;
        }

        return (['img'=>$img, 'largeImg'=>$largeImg, 'imageId'=>$imageId]);
    }



    public function getFlickrAuthor($photoId)
    {
        $api_key = env('FLICKR_API');
        $url = env('FLICKR_PHOTO_INFO_URL');
        $url.= '&api_key='.$api_key;
        $url.= '&photo_id='.$photoId;
        $url.= '&format=json';
        $url.= '&nojsoncallback=1';
        $response = json_decode(file_get_contents($url));
        return $response;
//        $username = $response->photo->owner->username;
//        $license =  $response->photo->license;

    }

}