<?php

namespace App;

use Acme\Likeability;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Http\Request;
use Session;
use Tymon\JWTAuth\Facades\JWTAuth;

class Nomination extends Model
{

    use Likeability;


    protected $fillable = ['user_id', 'name', 'place', 'description', 'latitude', 'longitude', 'options', 'location',
                            'funding', 'campaign_id', 'featured', 'published'];

    protected $appends = ['renominations_count', 'renominated', 'raised'];


    /**
     * Get the options of the nomination
     * @return mixed
     */
    public function getOptionsAttribute()
    {
        return json_decode($this->attributes['options']);
    }


    /**
     * Get the funding details of the nomination
     * @return mixed
     */
    public function getFundingAttribute()
    {
        return json_decode($this->attributes['funding']);
    }


    /**
     * Get the location details of the nomination
     * @return mixed
     */
    public function getLocationAttribute()
    {
        return json_decode($this->attributes['location']);
    }


    /**
     * Get the renominations count.
     * @return int
     */
    public function getRenominationsCountAttribute()
    {
        return count($this->renominations);
    }

    /**
     * Check if the nomination is renominated by the logged in user or not.
     * @return int
     */
    public function getRenominatedAttribute()
    {
        return $this->isRenominated();
    }



    /**
     * Get the total amount raised for this nomination
     * @return mixed
     */
    public function getRaisedAttribute()
    {
        return $this->backers()->sum('amount');
    }


    public function getUrlAttribute()
    {
        return ('nominations/'.$this->attributes['id'].'/'.str_slug($this->attributes['name']));
    }


    /**
     * Get the owner of the nomination
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Get the updates from the nomination
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function updates()
    {
        return $this->hasMany(Update::class);
    }


    /**
     * Nomination belongs to a Campaign
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }


    /**
     * Get all the renominations for the Nomination.
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function renominations()
    {
        return $this->morphMany(Like::class, 'likeable');
    }


    /**
     * List of all renominated users for a nomination
     * @param null $limit
     * @return mixed
     */
    public function renominated_users($limit=null)
    {
        return $this->likes()->with('user')->take($limit)->get();
    }



    /**
     * Get the list of all backers for the nomination.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function backers()
    {
        return $this->hasMany('App\Backers', 'campaign_id', 'campaign_id');
    }


    /**
     * Get the whole collection of backers from a campaign.
     * @return mixed
     */
    public function getCollection()
    {
        return (Backers::where('campaign_id', $this->id)->sum('amount'));
    }


    /**
     * Check if the nomination is renominated or not.
     * @return bool
     */
    public function isRenominated()
    {
        return $this->isLiked();
    }


    /**
     *  Toggle Renomination
     */
    public function toggleRenomination()
    {
        $this->toggle();
    }



    public static function createNew(Request $request)
    {
        $auth = JWTAuth::parseToken()->authenticate();

        return self::create([
            'user_id' => $auth->id,
            'campaign_id' => null,
            'name' => $request->input('name'),
            'place' => $request->input('place'),
            'description' => $request->input('description'),
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude'),
            'featured' => false,
            'published' => true,
            'options' => json_encode([
                'branding' => [
                    'type' => $request->input('branding_type'),
                    'font' => $request->input('font'),
                    'logo' => $request->input('logo')
                ],
                'image' => [
                    'type' => $request->input('image_type'),
                    'path' => $request->input('path'),
                    'offset' => [
                        'cover' => $request->input('cover_offset'),
                        'box' => $request->input('box_offset')
                    ],
                    'owner' => $request->input('flickr_id'),
                    'licence' => $request->input('license')
                ]
            ]),

            'funding' => json_encode([
                'type' => null,
                'amount' => null,
                'period' => 0
            ]),

            'location' => json_encode([
                'place' => $request->input('place'),
                'type' => $request->input('property_type'),
                'lng' => $request->input('latitude'),
                'lat' => $request->input('longitude'),
                'address' =>  [
                    'country' => $request->input('country'),
                    'state' => $request->input('state'),
                    'city' => $request->input('city')
                ]
            ])

        ]);
    }


    /**
     * Update the nomination
     *
     * @param Request $request
     * @return bool|int
     */
    public function updateNomination(Request $request)
    {

        return self::update([
            'campaign_id' => null,
            'name' => $request->has('name') ? $request->input('name') : $this->name,
            'place' => $request->has('place') ? $request->input('place') : $this->place,
            'description' => $request->has('description') ? $request->input('description') : $this->description,
            'latitude' => $request->has('latitude') ? $request->input('latitude') : $this->latitude,
            'longitude' => $request->has('longitude') ? $request->input('longitude') : $this->longitude,
            'options' => json_encode([
                'branding' => [
                    'type' => $request->has('branding_type') ? $request->input('branding_type') : $this->options->branding->type,
                    'font' => $request->has('font') ? $request->input('font') : $this->options->branding->font,
                    'logo' => $request->has('logo') ? $request->input('logo') : $this->options->branding->logo
                ],
                'image' => [
                    'type' => $request->has('image_type') ? $request->input('image_type') : $this->options->image->type,
                    'path' => $request->has('path') ? $request->input('path') : $this->options->image->path,
                    'offset' => [
                        'cover' => $request->has('cover_offset') ? $request->input('cover_offset') : $this->options->image->offset->cover,
                        'box' => $request->has('box_offset') ? $request->input('box_offset') : $this->options->image->offset->box
                    ],
                    'owner' => $request->has('flickr_id') ? $request->input('flickr_id') : $this->options->image->owner,
                    'licence' => $request->has('license') ? $request->input('license') : $this->options->image->licence
                ]
            ]),

            'funding' => json_encode([
                'type' => null,
                'amount' => null,
                'period' => 0
            ]),

            'location' => json_encode([
                'place' => $request->has('place') ? $request->input('place') : $this->location->place,
                'type' => $request->has('property_type') ? $request->input('property_type') : $this->location->type,
                'lng' => $request->has('latitude') ? $request->input('latitude') : $this->location->lng,
                'lat' => $request->has('longitude') ? $request->input('longitude') : $this->location->lat,
                'address' =>  [
                    'country' => $request->has('country') ? $request->input('country') : $this->location->address->country,
                    'state' => $request->has('state') ? $request->input('state') : $this->location->address->state,
                    'city' => $request->has('city') ? $request->input('city') : $this->location->address->city
                ]
            ])

        ]);
    }
}
