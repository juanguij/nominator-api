<?php namespace App;

use App\Cards;

class Payment
{


    /**
     * Add a new credit/debit card to user account.
     * @param $payload
     */
    public function newCard($payload)
    {
        $cards = auth()->user()->cards;

        if(count($cards) == 0)
        {
            $this->newCustomer($payload['stripe_token']);
        }
        else
        {
            $this->existingCustomer($payload['stripe_token'], $cards);
        }

    }


    /**
     * If the user does not have any card, create a new customer
     * and add the card under that customer.
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function newCustomer($token)
    {
        $card_result = (new Cards())->createCustomer(auth()->user()->email, $token);

        if(!$card_result['success'])
        {
            return response()->json([$card_result['error'], 422]);
        }

        (new Cards())->createNewCardFromCustomer(auth()->user(), $card_result['customer']);
    }




    /**
     * If the user already have a card and a customer account,
     * add the card under that customer.
     * @param $token
     * @param $cards
     * @return mixed
     */
    protected function existingCustomer($token, $cards)
    {

        $card_result = (new Cards())->addNewCardToExistingCustomer(auth()->user(), $token, $cards[0]->customer_id);

        if(!$card_result['success'])
        {
            return Response::json(['success'=>true, 'msg'=>$card_result['error']]);
        }

        (new Cards())->createNewCardFromCard(auth()->user(), $card_result['card']);

    }


}