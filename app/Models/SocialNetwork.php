<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    protected $table = 'social_networks';

    protected $fillable = ['facebook_id', 'fb_name', 'tw_name'];
}
