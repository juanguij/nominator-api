<?php
namespace App;

use App;
use Auth;
use Acme\Billing;
use App\Billing\StripeGateway;
use Acme\Billing\BillingInterface;
use Illuminate\Database\Eloquent\Model;
use Stripe\Card;

class Cards extends Model{

    protected $table = 'cards';

    protected $fillable = ['card_id', 'customer_id', 'exp_year', 'exp_month', 'funding', 'brand', 'last4'];

    protected $card_id;

    public $gateway;



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->gateway = new StripeGateway();
    }


    public function createNewCardFromCustomer(User $user, $customer)
    {

        return $user->cards()->create([
            'customer_id' => $customer->id,
            'card_id' => $customer['sources']['data'][0]->id,
            'last4' => $customer['sources']['data'][0]->last4,
            'brand' => $customer['sources']['data'][0]->brand,
            'funding' => $customer['sources']['data'][0]->funding,
            'exp_month' => $customer['sources']['data'][0]->exp_month,
            'exp_year' => $customer['sources']['data'][0]->exp_year
        ]);

    }


    public function createNewCard(User $user, $card_id, $customer_id, $billing_info)
    {
        return $user->cards()->create([
            'card_id' => $card_id,
            'customer_id' => $customer_id,
            'exp_year' => $billing_info['billing']['source']->exp_year,
            'exp_month' => $billing_info['billing']['source']->exp_month,
            'funding' => $billing_info['billing']['source']->funding,
            'brand' => $billing_info['billing']['source']->brand,
            'last4' => $billing_info['billing']['source']->last4
        ]);

    }


    public function createNewCardFromCard(User $user, $card)
    {
        return $user->cards()->create([
            'card_id' => $card->id,
            'customer_id' => $card->customer,
            'exp_year' => $card->exp_year,
            'exp_month' => $card->exp_month,
            'funding' => $card->funding,
            'brand' => $card->brand,
            'last4' => $card->last4
        ]);

    }

    public function createCustomer($email, $stripe_token)
    {
        return $this->gateway->createCustomer($email, $stripe_token);
    }


    public function checkForCard($billing_info)
    {
        if($billing_info['success']!=false)
        {

            $card = $this->getCard(Auth::id(), $billing_info['billing']['source']->id, $billing_info['billing']['source']->customer);

            if(count($card)==0)
            {
                $this->createNewCard(Auth::user(), $billing_info['billing']['source']->id, $billing_info['billing']['source']->customer, $billing_info);
            }
            return true;
        }
        else
            return false;
    }


    public function findCard($user_id, $stripe_card_id)
    {
        return Cards::where('user_id', $user_id)
            ->where('card_id', $stripe_card_id)->first();
    }


    public function getCard($user_id, $stripe_card_id, $customer_id)
    {
        return Cards::where('user_id', $user_id)
            ->where('card_id', $stripe_card_id)
            ->where('customer_id', $customer_id)->first();
    }



    public function payWithNewCard($amount, $email, $stripe_token, $currency)
    {
        try {
            $billing_info = $this->gateway->charge($email, $stripe_token, $amount, $currency);

            return $billing_info;
        }
        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }


    public function payWithStripeCard($customer_id, $card_id, $amount, $currency)
    {
        try
        {
            $billing_info = $this->gateway->chargeExisting($customer_id, $card_id, $amount, $currency);

            return $billing_info;

        } catch (Exception $e)
        {
            return $e->getMessage();
        }
    }



    public function addNewCardToExistingCustomer(User $user, $stripe_token, $customer_id)
    {
        return $this->gateway->newCard($customer_id, $stripe_token);
    }



    public function deleteCard(User $user, $card_id, $customer_id)
    {
        $delete = $this->gateway->deleteCard($customer_id, $card_id);

        Cards::where('card_id', $card_id)->where('user_id', $user->id)->delete();

        return $delete;
    }



}