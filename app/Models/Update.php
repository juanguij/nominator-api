<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    protected $with = ['user'];

    protected $fillable = ['user_id', 'message'];


    /**
     * Get the user for the update
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get the nomination corresponding to the particular update
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nomination()
    {
        return $this->belongsTo(Nomination::class);
    }
}
