<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\NewFollower;
use Acme\Likeability;

class Like extends Model
{

    use Likeability;


    protected  $hidden = ['likeable_id', 'likeable_type'];

    protected $fillable = ['user_id'];

    protected $with = ['user'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public function follower()
    {
        return $this->belongsTo('App\User', 'likeable_id');
    }


    public function notify()
    {
        return event(new NewFollower($this->likeable_id));
    }

    public function likeables()
    {
        return $this->morphToMany();
    }



}
