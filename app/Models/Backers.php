<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backers extends Model
{
    protected $table = 'backers';

    protected $fillable = ['name', 'initial', 'email', 'hide_name', 'message',
                            'amount', 'address', 'city', 'state', 'country',
                            'postal_code', 'payment_id'
                            ];

    protected $appends = ['formatted_time'];


    /**
     * Get the nomination belonging to the current donation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function nomination()
    {
        return $this->belongsTo('App\Nomination', 'campaign_id', 'campaign_id');
    }


    /**
     * Get formatted time for the backer.
     * @return mixed
     */
    public function getFormattedTimeAttribute()
    {
        return $this->created_at->diffForHumans();
    }


    /**
     * Get the whole collection of backers from a campaign.
     * @param $id
     * @return mixed
     */
    public static function getCollection($id)
    {
        return Backers::where('campaign_id', $id)->sum('amount');
    }


}