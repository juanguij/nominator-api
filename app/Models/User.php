<?php

namespace App;

use Acme\Likeability;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    use Likeability;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'initial'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }



    protected $with = ['cards', 'social'];


//    protected $appends = ['liked', 'text_avatar', 'avatar'];


    /**
     * Get the list of all social connections
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function social()
    {
        return $this->hasOne('App\SocialNetwork');
    }


    /**
     * Get the list of all credit cards
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany('App\Cards');
    }


    /**
     * Get all the nominations from the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nominations()
    {
        return $this->hasMany('App\Nomination');
    }


    /**
     * Get the list of donations made by current user.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function backers()
    {
        return $this->hasMany('App\Backers', 'email', 'email');
    }


    /**
     * Get the list of all renominations by the current user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function renominations()
    {
        return Nomination::with('user')
            ->whereHas('likes', function ($query){
                $query->where('user_id', $this->id);
            })->get();
    }


    /**
     * Get if the user has renominated this nomination
     * @param $renoms
     * @return mixed
     */
    public function checkAuthRenominated($renoms)
    {

        return Nomination::with('user')
            ->whereIn('id', $renoms->lists('id'))
            ->whereHas('likes', function($query){
                return $query->where('user_id', $this->id);
            })
            ->get();
    }


    /**
     * Follow a user by Authenticated User
     * @return mixed
     */
    public function follow()
    {
        return $this->like();
    }


    /**
     * Unfollow a user by the authenticated user
     * @return mixed
     */
    public function unfollow()
    {
        return $this->unlike();
    }


    /**
     * Check if the current user is following auth user.
     * @return mixed
     */
    public function isFollowing()
    {
        return $this->isLiked();
    }


    /**
     * Toggle following a user
     * @return mixed
     */
    public function toggleFollow()
    {
        return $this->toggle();
    }


    /**
     * Check if the authenticated user follows this user.
     * @return mixed
     */
    public function getLikedAttribute()
    {
        return $this->isFollowing();
    }



    /**
     * Get the list of all followers for the current user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followers()
    {
        return $this->morphMany('App\Like', 'likeable')->with('user');
    }


    /**
     * Get the list of all people who are following current user.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function following()
    {
        return $this->morphMany('App\Like', 'user', 'likeable_type')->with('follower');
    }


    /**
     * Get the avatar of the user
     * @return string
     */
    public function getAvatarAttribute()
    {
        return avatar($this);
    }


    /**
     * Check if the avatar is text type.
     * @return bool
     */
    public function getTextAvatarAttribute()
    {
        return (!$this->custom_dp && $this->facebook_id==null);
    }
}