<?php

namespace Acme;

use App\Like;
use Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

trait Likeability
{

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function like()
    {
        $like = new Like(['user_id' => $this->getAuthId()]);
        return $this->likes()->save($like);
    }

    public function isLiked()
    {
        return !! $this->likes()->where('user_id', $this->getAuthId())->count();
    }

    public function unlike()
    {
        return $this->likes()->where('user_id', $this->getAuthId())->delete();
    }

    public function toggle()
    {
        if($this->isLiked())
            return $this->unlike();
        return $this->like();
    }


    protected function getAuthId()
    {
        return (JWTAuth::getToken()) ? JWTAuth::parseToken()->authenticate()->id : null;
    }

}